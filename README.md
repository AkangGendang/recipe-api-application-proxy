# Recipe App API proxy


NGINX proxy app for new recipe app API

## Usage

### Environtment Variable 

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward request to (default: `app`)
 * `APP_PORT` - Port of the app to forward request to (default: `9000`)